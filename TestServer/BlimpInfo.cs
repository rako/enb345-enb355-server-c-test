﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestServer
{
    class BlimpInfo
    {
        public class Vector3
        {
            public float X { get; set; }
            public float Y { get; set; }
            public float Z { get; set; }
        }

        public class KinematicState
        {
            public Vector3 Attitude { get; set; }
            public Vector3 Position { get; set; }
            public Vector3 Velocity { get; set; }
            public Vector3 RollRates { get; set; }
        }

        public enum OperatingMode
        {
            WallFollowing,
            SearchingForSurvivors,
            Waiting,
            MovingToWaypoint,
            PayLoadDelivery,
            Takeoff,
            Landing,
            Test,
            Initialising
        }

        public class AutoPilotDataOutput
        {
            public KinematicState kinematicState { get; set; }
            public Vector3 Waypoint { get; set; }
            public OperatingMode operatingMode { get; set; }
            public Vector3 ThrustRequired { get; set; }
        }

        public string[] OperatingModeStrings = { "Wall Following", "Searching For Survivors", "Waiting", "Moving To Waypoint", "PayLoad Delivery", "Takeoff", "Landing", "Test", "Initialising" };

        public static AutoPilotDataOutput data { get; set; }

    }
}
