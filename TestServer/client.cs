﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace TestServer
{
    class Client
    {
        //todo implement a class for a client

        private TcpClient _tcpClient;
        private bool _isActive;
        private LinkedListNode<byte[]> _lastCommand;
        private string _name;
        private string _ip;

        public TcpClient tcpClient
        {
            get { return _tcpClient; }
        }

        public bool isActive
        {
            get { return _isActive; }
        }

        public string name
        {
            get { return _name; }
        }

        public string ip
        {
            get { return _ip; }
        }

        public LinkedListNode<byte[]> lastCommand
        {
            get { return _lastCommand; }
            set { _lastCommand = value; }
        }

        public Client(TcpClient tcpClient, bool isActive, string name, string ip, LinkedListNode<byte[]> lastCommand = null)
        {
            _tcpClient = tcpClient;
            _isActive = isActive;
            _lastCommand = lastCommand;
            _name = name;
            _ip = ip;
        }
    }
}
