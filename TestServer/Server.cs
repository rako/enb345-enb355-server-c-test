﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.Diagnostics;
using System.Drawing;
using ENB354_ENB355_GCS_COMMON;

namespace TestServer
{
    class Server
    {

        #region veriables
        private readonly int PACKET_SEND_INTERVAL;//ms
        private readonly int SIMULATION_UPDATE_INTERVAL;//ms
        private readonly int PORT_NUMBER;

        private TcpListener tcpListener;
        private Thread listenThread;
        private Thread sendThread;
        private Thread simulateThread;//thread to simulate blimp
        private Thread adminThread;
        private bool _stop = false;

        private Dictionary<string, Data> myDataToSend = new Dictionary<string, Data>();
        private CommandLog commandLog = new CommandLog();

        private byte[] setupPacket;

        private static Bitmap[] images = { Images.earth0001, Images.earth0003, Images.earth0005, Images.earth0007, Images.earth0009, Images.earth0011, Images.earth0013, Images.earth0015, Images.earth0017, Images.earth0019, Images.earth0021, Images.earth0023, Images.earth0025, Images.earth0027, Images.earth0029, Images.earth0031, Images.earth0033, Images.earth0035, Images.earth0037, Images.earth0039, Images.earth0041, Images.earth0043, Images.earth0045, Images.earth0047, Images.earth0049, Images.earth0051, Images.earth0053, Images.earth0055, Images.earth0057, Images.earth0059, Images.earth0061, Images.earth0063, Images.earth0065, Images.earth0067, Images.earth0069, Images.earth0071, Images.earth0073, Images.earth0075, Images.earth0077, Images.earth0079, Images.earth0081, Images.earth0083, Images.earth0085, Images.earth0087 };
        const string AdminGCSName = "Group 4 - GCS";
        
        private Client adminClient = null;


        private int currentImage = 0;

        private LinkedList<Client> clients;


        const byte keepAlliveTimeInSeconds = 2;

        private bool _isJpeg;
        #endregion

        #region constuctor
        public Server(int portNumber, bool isJpeg = false, int delay = 200)
        {
            _isJpeg = isJpeg;
            PACKET_SEND_INTERVAL = delay;
            SIMULATION_UPDATE_INTERVAL = delay;
            PORT_NUMBER = portNumber;

            //get all data setup and running
            addData();

            //todo spawn admin thread

            //todo change values (simulation)

            //add commands to simulation

            //todo multiple ports for either jpeg, video or nothing

            //todo c++ unix

            //todo a bit more thread safety but wait untill c++ to worry about that crap

            setupPacket = GenerateSetUpPacket();
            clients = new LinkedList<Client>();
            this.tcpListener = new TcpListener(IPAddress.Any, PORT_NUMBER);
            this.listenThread = new Thread(new ThreadStart(ListenForClients));
            this.listenThread.Start();
            this.sendThread = new Thread(new ThreadStart(SendData));
            this.sendThread.Start();
            this.simulateThread = new Thread(new ThreadStart(simulate));
            this.simulateThread.Start();
            this.adminThread = new Thread(new ThreadStart(admin));
            this.adminThread.Start();
            commandLog.add(ENB354_ENB355_GCS_COMMON.Command.CommandSource.Blimp, "Server succesffully started");
        }
        #endregion

        #region admin
        private void admin()
        {
            while (!_stop)
            {
                
                try
                {
                    if (adminClient != null && adminClient.tcpClient.Connected)
                    {
                        NetworkStream adminStream = adminClient.tcpClient.GetStream();
                        byte[] header = Helper.read(adminStream, Networking.HeaderSize);
                        UInt32 size = BitConverter.ToUInt32(header, 1);
                        byte[] body = Helper.read(adminStream, (int)size - Networking.HeaderSize);

                        switch ((PacktHeader)header[0])
                        {
                            case PacktHeader.CommandBlimp:
                                //data Packet
                                Command(body);
                                break;
                            case PacktHeader.Deliver:
                                //commands packet
                                DeliverPayload(body);
                                break;
                            default:
                                throw new Exception("Incorrect admin packet: " + header[0]);
                        }
                        
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("admin error: " + ex.Message);
                    adminClient.tcpClient.Close();
                }
                Thread.Sleep(SIMULATION_UPDATE_INTERVAL);
            }
        }

        private void DeliverPayload(byte[] body)
        {

            throw new NotImplementedException();
        }

        private void Command(byte[] body)
        {
            if (body.Length != 1)
            {
                throw new Exception("Incorrect command packet size");
            }
            switch ((ENB354_ENB355_GCS_COMMON.CommandValue)body[0])
            {
                case ENB354_ENB355_GCS_COMMON.CommandValue.Circumnavigate:
                    break;
                case ENB354_ENB355_GCS_COMMON.CommandValue.Land:
                    break;
                case ENB354_ENB355_GCS_COMMON.CommandValue.Search:
                    break;
                case ENB354_ENB355_GCS_COMMON.CommandValue.TakeOff:
                    break;
                default:
                    throw new Exception("Invalid command recieved from admin");
            }
        }
        #endregion

        #region simulation
        private void simulate()
        {
            int randomInt = 0;
            while (!_stop)
            {
                currentImage++;
                randomInt++;
                if (currentImage == images.Length)
                {
                    currentImage = 0;
                }
                if (_isJpeg)
                {
                    myDataToSend["Imagery"].Update(images[currentImage]);
                }
                myDataToSend["randomInt"].Update(randomInt);

                //todo simulate movements

                Thread.Sleep(SIMULATION_UPDATE_INTERVAL);
            }
        }
        #endregion

        #region add data
        private void addData()
        {
            myDataToSend.Add("X pos (meteres)", new Data("X pos (meteres)", 0.0f));
            myDataToSend.Add("Y pos (meteres)", new Data("Y pos (meteres)", 0.0f));
            myDataToSend.Add("X velocity (meteres/second)", new Data("X velocity (meteres/second)", 0.0f));
            myDataToSend.Add("Y velocity (meteres/second)", new Data("Y velocity (meteres/second)", 0.0f));
            if (_isJpeg)
            {
                myDataToSend.Add("Imagery", new Data("Imagery", images[currentImage]));
            }
            myDataToSend.Add("Status", new Data("Status", 0));
            myDataToSend.Add("randomInt", new Data("randomInt", 0));
            myDataToSend.Add("randomChar", new Data("randomInt", (byte)0x43));
        }
        #endregion

        #region generate sensor data packet
        private byte[] GenerateSensorDataPacket()
        {
            byte[] message;
            UInt32 pos = 0;
            UInt32 PacketSize = 0;

            LinkedList<byte[]> dataOfSensors = new LinkedList<byte[]>();
            UInt32 dataOfSensorsSize = 0;//size of dataOfSensors in bytes including overhead


            byte[] tempSensorData;

            foreach (KeyValuePair<string, Data> pair in myDataToSend)
            {
                tempSensorData = pair.Value.toBytes();
                dataOfSensors.AddLast(tempSensorData);
                dataOfSensorsSize += (UInt32)tempSensorData.Length;
            }

            PacketSize = 1 + 4 + dataOfSensorsSize;
            message = new byte[PacketSize];

            message[pos] = (byte)PacktHeader.SensorData;
            pos += 1;
            BitConverter.GetBytes(PacketSize).CopyTo(message, pos);
            pos += 4;

            foreach (byte[] SensorData in dataOfSensors)
            {
                SensorData.CopyTo(message, pos);
                pos += (UInt32)SensorData.Length;
            }

            Debug.Assert(pos == PacketSize, "error calculating te size of sensor data packet");

            return message;
        }
        #endregion

        #region generate commands data packet
        private byte[] GenerateCommandsDataPacket(ref LinkedListNode<byte[]> lasCommand)
        {
            byte[] commandLogBody = commandLog.toBytes(ref lasCommand);

            if (commandLogBody.Length == 0)
            {
                return new byte[0];
            }
            UInt32 packetSize = (UInt32)commandLogBody.Length + 5;
            byte[] packet = new byte[packetSize];
            int pos  = 0;
            packet[pos] = (byte)PacktHeader.Commands;
            pos +=1;
            BitConverter.GetBytes(packetSize).CopyTo(packet, pos);
            pos += 4;
            commandLogBody.CopyTo(packet, pos);
            return packet;
        }
        #endregion

        #region send data
        private void SendData() {
            LinkedListNode<byte[]> lastCommand = null;
            byte[] commandsPacket = null;
            while (!_stop) 
            {
                if (clients.Count > 0)
                {
                    //generate packet
                    byte[] sensorDataPacket = GenerateSensorDataPacket();
                    LinkedListNode<byte[]> previousCommand = lastCommand;
                    


                    //send packet
                    NetworkStream clientStream;
                    for (LinkedListNode<Client> clientNode = clients.First; clientNode != null; clientNode = clientNode.Next)
                    {
                        if (clientNode.Value.tcpClient.Connected)
                        {
                            try
                            {
                                clientStream = clientNode.Value.tcpClient.GetStream();
                                clientStream.Write(sensorDataPacket, 0, sensorDataPacket.Length);
                                if (clientNode.Value.lastCommand == previousCommand)
                                {
                                    if (commandsPacket == null)
                                    {
                                        //only generate if needed and only needs to be generated once
                                        commandsPacket = GenerateCommandsDataPacket(ref lastCommand);
                                    }
                                    if (commandsPacket.Length > 0)
                                    {
                                        clientStream.Write(commandsPacket, 0, commandsPacket.Length);
                                    }
                                    clientNode.Value.lastCommand = lastCommand;
                                }
                                else
                                {
                                    LinkedListNode<byte[]> tempClientLastCommand = clientNode.Value.lastCommand;
                                    byte[] IndividualCommandsPacket = GenerateCommandsDataPacket(ref tempClientLastCommand);
                                    clientNode.Value.lastCommand = tempClientLastCommand;
                                    if (IndividualCommandsPacket.Length > 0)
                                    {
                                        clientStream.Write(IndividualCommandsPacket, 0, IndividualCommandsPacket.Length);
                                    }
                                }
                                
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("Error with: " + clientNode.Value.name + " - Error Message: " + ex.Message);
                                Console.WriteLine("Disconnected: " + clientNode.Value.name);
                                if (clientNode.Value.isActive)
                                {
                                    commandLog.add(ENB354_ENB355_GCS_COMMON.Command.CommandSource.Blimp, "Active GCS Disconnected: " + clientNode.Value.ip);
                                }
                                clientNode.Value.tcpClient.Close();
                                clients.Remove(clientNode);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Disconnected: " + clientNode.Value.name);
                            if (clientNode.Value.isActive)
                            {
                                commandLog.add(ENB354_ENB355_GCS_COMMON.Command.CommandSource.Blimp, "Active GCS Disconnected: " + clientNode.Value.ip);
                            }
                            clientNode.Value.tcpClient.Close();
                            
                            clients.Remove(clientNode);
                        }
                    }


                }
                commandsPacket = null;//free memory
                Thread.Sleep(PACKET_SEND_INTERVAL);
            }
            
        }

        
        #endregion

        #region listen for clients
        private void ListenForClients()
        {
            this.tcpListener.Start();
            Console.WriteLine("Listening on: " + Helper.getLocalIP() + ":" + PORT_NUMBER);
            while (!_stop)
            {
                //blocks until a client has connected to the server
                TcpClient client = this.tcpListener.AcceptTcpClient();

                //create a thread to handle communication with connected client
                Thread clientThread = new Thread(new ParameterizedThreadStart(HandleClientComm));
                clientThread.Start(client);
            }
        }
        #endregion

        #region start client comm
        #region handle new client
        private void HandleClientComm(object client)
        {
            TcpClient tcpClient = null;
            try
            {
                tcpClient = (TcpClient)client;
                NetworkStream clientStream = tcpClient.GetStream();

                Console.WriteLine("Connected Remote: " + tcpClient.Client.RemoteEndPoint + " - Local: " + tcpClient.Client.LocalEndPoint);

                //todo put login logic here
                string ClientName = null;
                byte error = recieveLoginRequest(clientStream, out ClientName);


                //send login reply packet
                Client loginClient = null;

                if (error == (byte)ErrorCodes.NoError)
                {
                    if ((ClientName == null || ClientName.Length == 0))
                    {
                        ClientName = tcpClient.Client.RemoteEndPoint.ToString();
                    }
                    //check if admin
                    if (AdminGCSName == ClientName)
                    {
                        error = (byte)ErrorCodes.AdminLogin;
                        if (adminClient != null && adminClient.tcpClient.Connected)
                        {
                            adminClient.tcpClient.Close();
                        }
                        loginClient = new Client(tcpClient, true, ClientName, tcpClient.Client.RemoteEndPoint.ToString());
                        adminClient = loginClient;
                        commandLog.add(ENB354_ENB355_GCS_COMMON.Command.CommandSource.GCS, "Active GCS Sent Login Request from: " + tcpClient.Client.RemoteEndPoint);
                    }
                    else
                    {
                        loginClient = new Client(tcpClient, false, ClientName, tcpClient.Client.RemoteEndPoint.ToString());
                    }
                }


                byte[] reply = new byte[1 + 4 + 1 + 1];
                reply[0] = (byte)PacktHeader.ReplyLoginRequest;
                BitConverter.GetBytes(reply.Length).CopyTo(reply, 1);
                reply[5] = error;
                reply[6] = keepAlliveTimeInSeconds;

                clientStream.Write(reply, 0, reply.Length);

                //send set up and add to list
                if (error == (byte)ErrorCodes.NoError || error == (byte)ErrorCodes.AdminLogin)
                {
                    clientStream.Write(setupPacket, 0, setupPacket.Length);

                    clients.AddLast(loginClient);
                    Console.WriteLine("Client Connected: " + ClientName + "\nClient IP: " + tcpClient.Client.RemoteEndPoint);
                    if (loginClient.isActive)
                    {
                        commandLog.add(ENB354_ENB355_GCS_COMMON.Command.CommandSource.Blimp, "Accepted login Active Login Request from: " + tcpClient.Client.RemoteEndPoint);
                    }
                }
                else
                {
                    tcpClient.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                try
                {
                    Console.WriteLine("Error on: " + tcpClient.Client.RemoteEndPoint);
                    tcpClient.Close();

                }
                catch { }
            }
            
        }
        #endregion

        #region recieve login request
        private byte recieveLoginRequest(NetworkStream clientStream, out string ClientName)
        {
            byte error = (byte)ErrorCodes.NoError;
            ClientName = string.Empty;
            try
            {
                //get login packet
                byte[] buffer;

                buffer = Helper.read(clientStream, 5);
                byte packetHeader = buffer[0];
                if (packetHeader != (byte)PacktHeader.LoginRequest)
                {
                    return error = (byte)ErrorCodes.UnknownTypeOfPacket;
                }
                UInt32 size = BitConverter.ToUInt32(buffer, 1);
                if (size < 6)
                {
                    return error = (byte)ErrorCodes.DataPacketSizeMismatch;
                }
                buffer = Helper.read(clientStream, (int)size - 5);
                int pos = 0;
                ClientName = Helper.getStringFromBytes(buffer, ref pos);

                if (buffer.Length != ClientName.Length+1)
                {
                    return error = (byte)ErrorCodes.DataPacketSizeMismatch;
                }
            }
            catch
            {
                return error = (byte)ErrorCodes.DataPacketSizeMismatch;
            }

            return error;
        }
        #endregion
        #endregion

        #region generate set up packet
        private byte[] GenerateSetUpPacket() {
            byte[] message;
            UInt32 pos = 0;
            UInt32 PacketSize = 0;

            byte N = (byte)myDataToSend.Count;

            byte[] dataTypes = new byte[N];

            LinkedList<byte[]> limits = new LinkedList<byte[]>();
            UInt32 limitsSize = 0;//size of limits in bytes including overhead

            LinkedList<byte[]> names = new LinkedList<byte[]>();
            UInt32 namesSize = 0;//size of names in bytes including overhead


            int count = 0;
            byte[] tempName;
            byte[] tempLimit;
            foreach (KeyValuePair<string, Data> pair in myDataToSend)
            {
                dataTypes[count] = (byte)pair.Value.dataType;
                if (pair.Value.dataType == Data.DataType.SI32 || pair.Value.dataType == Data.DataType.FLOAT)
                {
                    tempLimit = pair.Value.MinToBytes();
                    limits.AddLast(tempLimit);
                    limitsSize += (UInt32)tempLimit.Length;
                    tempLimit = pair.Value.MaxToBytes();
                    limits.AddLast(tempLimit);
                    limitsSize += (UInt32)tempLimit.Length;
                }
                tempName = Helper.StringToByteArray(pair.Key);
                names.AddLast(tempName);
                namesSize += (UInt32)tempName.Length;
                count++;
            }

            PacketSize = 1 + 4 + 1 + (UInt32)N + limitsSize + namesSize;
            message = new byte[PacketSize];

            message[pos] = (byte)PacktHeader.SensorDataStructure;
            pos += 1;
            BitConverter.GetBytes(PacketSize).CopyTo(message, pos);
            pos += 4;
            message[pos] = N;
            pos += 1;
            dataTypes.CopyTo(message, pos);
            pos += N;

            foreach (byte[] limit in limits)
            {
                limit.CopyTo(message, pos);
                pos += (UInt32)limit.Length;
            }

            foreach (byte[] name in names)
            {
                name.CopyTo(message, pos);
                pos += (UInt32)name.Length;
            }

            Debug.Assert(pos == PacketSize, "error calculating te size of set up packet");

            return message;
        }
        #endregion

        #region stop threads and exit
        internal void stop()
        {
            _stop = true;
        }
        #endregion
    }
}
