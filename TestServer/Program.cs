﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Diagnostics;

namespace TestServer {
    class Program {
        private static Server server100HzJpeg;
        private static Server server5HzJpeg;
        private static Server server100HzRTP;
        private static Server server5HzRTP;

        static void Main(string[] args)
        {
            Console.WriteLine("Starting JPEG 100Hz server:");
            server100HzJpeg = new Server(3000, true, 1000 / 100);
            Thread.Sleep(100);
            Console.WriteLine("Starting JPEG 5Hz server:");
            server5HzJpeg = new Server(3001, true, 1000 / 5);
            Thread.Sleep(100);
            Console.WriteLine("Starting RTP 100Hz server:");
            server100HzRTP = new Server(3002, false, 1000 / 100);
            Thread.Sleep(100);
            Console.WriteLine("Starting RTP 5Hz server:");
            server5HzRTP = new Server(3003);
            Process.GetCurrentProcess().Exited += new EventHandler(Program_Exited);
        }

        private static void Program_Exited(object sender, EventArgs e)
        {
            server100HzJpeg.stop();
            server5HzJpeg.stop();
            server100HzRTP.stop();
            server5HzRTP.stop();
        }
    }
}
